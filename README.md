# Trilium extras

(I may want to give this project a new name).

My collection of [trilium] widgets/plugins.

Some plugins here are adapted/copied from the demo tree.

[trilium]: https://github.com/zadam/trilium

# Install instruction

For each plugins, there will be a `README.md` file describing what the plugin will do
and how to install them. Please consult that document for install instruction.

# List of plugins here

- Meeting notes: Organize meeting notes by day and project
- Daily note: Some improvement for daily note
- Work hour tracker: Track everyday login and logout time
