/**
 * Plugin for trilium notes
 *
 * Script logic for the template
 *
 * Type: JS Frontend
 * Required attributes: None
 *
 * Just make sure this note is a direct child of "Template"
 * @module
 */

async function getTimes(month) {
  const days = await api.runOnBackend(
    async (month) => {
      function parseTime(date, time) {
        if (time == null) {
          return null;
        }

        let hour = null;
        let minute = null;

        if (time.length === 4) {
          hour = time.substring(0, 2);
          minute = time.substring(2, 4);
        } else {
          const splitted = time.split(":");
          if (splitted.length !== 2) {
            return null;
          }

          hour = splitted[0];
          minute = splitted[1];
        }

        return date
          .hour(parseInt(hour, 10))
          .minute(parseInt(minute, 10))
          .second(0)
          .millisecond(0);
      }

      const loginLabel = "loginTime";
      const logoutLabel = "logoutTime";
      const monthLabel = "monthNote";
      const dateLabel = "dateNote";

      const monthNote = await api.getNoteWithLabel(monthLabel, month);
      if (monthNote == null) {
        return [];
      }

      const notes = await monthNote.getChildNotes();
      const days = [];

      for (const note of notes) {
        const date = api.dayjs(await note.getLabelValue(dateLabel));
        if (!date.isValid()) {
          continue;
        }

        const loginTime = parseTime(date, await note.getLabelValue(loginLabel));
        const logoutTime = parseTime(
          date,
          await note.getLabelValue(logoutLabel)
        );

        if (loginTime != null && logoutTime != null) {
          days.push({
            noteId: note.noteId,
            date: date.valueOf(),
            loginTime: loginTime.valueOf(),
            logoutTime: logoutTime.valueOf(),
            workHour: logoutTime.diff(loginTime, "hour", true).toFixed(2),
          });
        }
      }

      days.sort((a, b) => a.date - b.date);

      return days;
    },
    [month]
  );

  return Promise.all(
    days.map(async (day) => ({
      noteId: day.noteId,
      date: api.dayjs(day.date),
      loginTime: api.dayjs(day.loginTime),
      logoutTime: api.dayjs(day.logoutTime),
      workHour: parseFloat(day.workHour),
      noteLink: await api.createNoteLink(day.noteId, {
        title: api.dayjs(day.date).format("YYYY-MM-DD ddd"),
      }),
    }))
  );
}

async function getMonths() {
  const months = await api.runOnServer(async () => {
    const monthLabel = "monthNote";

    const monthNotes = await api.getNotesWithLabel(monthLabel);

    const monthLabels = await Promise.all(
      monthNotes.map((note) => note.getLabelValue(monthLabel))
    );

    return monthLabels
      .filter((month) => month != null)
      .filter((month) => month.split("-").length === 2);
  });

  months.sort((a, b) => b.localeCompare(a));

  return months;
}

/**
 * @returns {HTMLElement | null}
 */
function getRoot() {
  if (api.$container == null) {
    return null;
  }

  return api.$container[0];
}

async function refreshMonths() {
  const $root = getRoot();
  if ($root == null) {
    return;
  }

  const $monthSelect = $root.querySelector("#month-select");

  const months = await getMonths();
  const monthsDom = months.map((month) => {
    const el = document.createElement("option");
    el.setAttribute("value", month);
    el.textContent = month;
    return el;
  });

  $monthSelect.replaceChildren(...monthsDom);

  refreshTable();
}

function getSelectedMonth() {
  const $root = getRoot();
  if ($root == null) {
    return;
  }

  const $monthSelect = $root.querySelector("#month-select");
  const selectedIndex = $monthSelect.selectedIndex;
  if (selectedIndex === -1) {
    return null;
  }

  const $option = $monthSelect.options[selectedIndex];
  if ($option == null || $option.value == null) {
    return null;
  }
  return $option.value;
}

async function refreshTable() {
  const $root = getRoot();
  if ($root == null) {
    return;
  }

  const $tbody = $root.querySelector("#work-hour-table tbody");

  const selectedMonth = getSelectedMonth();
  const times = await getTimes(selectedMonth);
  const timesDom = times.map((time) => {
    const $row = document.createElement("tr");

    const $date = document.createElement("td");
    $($date).append(time.noteLink);

    const $loginTime = document.createElement("td");
    $loginTime.textContent = time.loginTime.format("HH:mm");

    const $logoutTime = document.createElement("td");
    $logoutTime.textContent = time.logoutTime.format("HH:mm");

    const $workHour = document.createElement("td");
    $workHour.textContent = `${time.workHour} (${time.workHour.toFixed()})`;

    $row.appendChild($date);
    $row.appendChild($loginTime);
    $row.appendChild($logoutTime);
    $row.appendChild($workHour);

    return $row;
  });

  $tbody.replaceChildren(...timesDom);
}

async function bootstrap() {
  const $root = getRoot();
  if ($root == null) {
    return;
  }

  await refreshMonths();
  $root
    .querySelector("#month-refresh")
    .addEventListener("click", refreshMonths);

  await refreshTable();
  $root.querySelector("#month-select").addEventListener("change", refreshTable);
}

bootstrap();
