/**
 * Plugin for trilium notes.
 *
 * This will fill in today note's login time with boot time - 1 minute
 *
 * Requirements:
 *   1. Windows machine. This is tested on Windows 7
 *   2. Powershell 7 (pwsh.exe) is installed
 *
 * This script is optional. If your machine does not meet the requirements, you can
 * safely skip this script's installation.
 *
 * Type: JS Backend
 * Required attributes:
 *   - `#run=backendStartup`
 */

const util = require("util");
const childProcess = require("child_process");
const exec = util.promisify(childProcess.exec);

const loginLabel = "loginTime";
const bootDuration = 1;
const cmd =
  "Get-CimInstance -Class Win32_OperatingSystem | Select-Object LastBootUpTime | ConvertTo-Json";

async function run() {
  const note = await api.getTodayNote();
  if (note == null) {
    return;
  }

  if (note.hasLabel(loginLabel)) {
    return;
  }

  const { stdout } = await exec(cmd, { shell: "pwsh.exe" });
  const bootTimeStr = JSON.parse(stdout).LastBootUpTime;

  const bootTime = api.dayjs(bootTimeStr);
  const now = api.dayjs();

  // Ensure bootTime equals to today
  if (!bootTime.isSame(now, "day")) {
    return;
  }

  const time = bootTime.subtract(bootDuration, "minutes").format("HH:mm");
  note.setLabel(loginLabel, time);
}

run();
