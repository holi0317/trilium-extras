# Widget description

This widget allows tracking of login and logout time everyday

This widget exist because my office requires me to log login and logout time.

# Behaviors

- Add fields in day note for recording login and logout time
- Present login time, logout time and work hour as a table
- When starting trilium, try to fill computer boot time as login time (Windows only)

# Labels used

These labels should only exist in `#dateNote`

| Label name    | Description                 |
| ------------- | --------------------------- |
| `#loginTime`  | Time for login at the date  |
| `#logoutTime` | Time for logout at the date |

For time format, both `HH:mm` and `HHmm` are supported.

Note that the time parser is very naive. Please don't try to break it.

# Install instruction

1. In "Day template", add following labels
   - `#label:loginTime=promoted,single,text`
   - `#label:logoutTime=promoted,single,text`
2. Create a "Work hour tracker" under "Statistics" folder (that folder exist in trilium demo notes)
3. Install "Template.html" template
4. Install "Template/script.js" script
5. (Optional) Install "Autofill login time" script
