/**
 * Plugin for trilium notes.
 *
 * This will add style for today's note. Allowing it to get highlighted.
 *
 * Type: JS Backend
 * Required attributes:
 *   - `#run=hourly`
 *   - `#run=backendStartup`
 */

const cssClass = "today";

const highlighted = api.getNotesWithLabel("cssClass", cssClass);
for (const note of highlighted) {
  note.removeLabel("cssClass", cssClass);
}

const todayNote = api.getTodayNote();
if (todayNote == null) {
  return;
}

todayNote.setLabel("cssClass", cssClass);
