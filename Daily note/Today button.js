/**
 * Plugin for trilium notes.
 *
 * This script is copied from trilium demo notes.
 *
 * This will add a "Today" button on toolbar that will focus on today's note
 *
 * Type: JS Frontend
 * Required attributes:
 *   - `#run=frontendStartup`
 */

api.addButtonToToolbar({
  title: "Today",
  icon: "calendar",
  shortcut: "alt+t",
  action: async function () {
    const todayNote = await api.getTodayNote();

    await api.waitUntilSynced();

    api.activateNote(todayNote.noteId);
  },
});
