# Widget description

Some improvement for daily note

# Behaviors

- A "Today" button on toolbar (From demo plugin set)
- Highlight today's note in tree

# Install instruction

All scripts/template have a header section containing how to install them. Please
refer to those instruction when installing.

This section only list out the install order.

1. (Recommended) Create a "Meeting notes" folder under "widget" folder
2. Install "Today button" script
3. Install "Bold today style" script
4. Install "Bold today logic" script
