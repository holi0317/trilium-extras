/**
 * Plugin for trilium notes.
 *
 * Dependency for "Attribute changed" script
 *
 * Type: JS Backend
 * Required attributes: (None)
 */

function getParentEpics(note) {
  const parents = note.getParentNotes();
  return parents.filter((parent) => parent.hasLabel("meetingEpicRoot"));
}

function clearParentEpic(note) {
  const parents = getParentEpics(note);

  for (const parent of parents) {
    api.ensureNoteIsAbsentFromParent(note.noteId, parent.noteId);
  }
}

/** @returns {Note | null} */
function underWhichEpic(note) {
  const parents = getParentEpics(note);

  if (parents.length === 0) {
    return null;
  }

  // We assume (and would like to hold this assumption) that a note can only be under
  // a single epic note.
  // In case of multiple parent notes, we would like to clear it out.
  if (parents.length > 1) {
    clearParentEpic(note);
    return null;
  }

  return parents[0];
}

function getEpicMeetingFolder(epic) {
  const children = epic.getChildNotes();
  const epicRoot = children.find((child) => child.hasLabel("meetingEpicRoot"));
  if (epicRoot != null) {
    return epicRoot;
  }

  const { note } = api.createTextNote(epic.noteId, "Meeting notes", "");
  note.setLabel("meetingEpicRoot");
  note.setLabel("iconClass", "bx bxs-chat");
  return note;
}

module.exports = function (note) {
  const epic = note.getOwnedRelationTarget("ProjectEpic");

  if (epic == null) {
    // Note has no epic. Clear all of them
    clearParentEpic(note);
    return;
  }

  const parentEpic = underWhichEpic(note);
  const epicMeetingFolder = getEpicMeetingFolder(epic);

  // The note is under correct epic
  if (parentEpic != null && parentEpic.noteId === epicMeetingFolder.noteId) {
    return;
  }

  api.ensureNoteIsPresentInParent(note.noteId, epicMeetingFolder.noteId);
};
