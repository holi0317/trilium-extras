/**
 * Plugin for trilium notes.
 *
 * Dependency for "Attribute changed" script
 *
 * Type: JS Backend
 * Required attributes: (None)
 */

/**
 * Remove all clone of the notes given that is under a date note
 *
 * @param {Note} note
 */
function clearParentDateNotes(note) {
  const parents = note.getParentNotes();
  const dateParents = parents.filter((parent) => parent.hasLabel("dateNote"));

  for (const parent of dateParents) {
    api.ensureNoteIsAbsentFromParent(note.noteId, parent.noteId);
  }
}

/**
 * Check which date note the given note is under.
 *
 * If the given note is child of multiple date note, clone of the given notes that is
 * under some date notes will be deleted and return `null`.
 *
 * If the given note is not a child of any date note, `null` will be returned
 *
 * Otherwise this will return the date (should be in `YYYY-MM-DD` format) of
 * the parent date note.
 *
 * @param {Note} note
 * @returns {string | null}
 */
function underWhichDateNote(note) {
  const parents = note.getParentNotes();
  const dateParents = parents.filter((parent) => parent.hasLabel("dateNote"));

  if (dateParents.length === 0) {
    return null;
  }

  // We assume (and would like to hold this assumption) that a note can only be under
  // a single date note.
  // In case of multiple parent notes, we would like to clear it out.
  if (dateParents.length > 1) {
    clearParentDateNotes(note);
    return null;
  }

  return dateParents[0].getLabelValue("dateNote");
}

/**
 * Ensure given meeting note is under correct date note.
 *
 * @param {Note} note
 */
module.exports = function (note) {
  const date = note.getLabelValue("Date");

  if (date == null || date === "") {
    // Date is empty. Ensure note is not under some date note
    clearParentDateNotes(note);
    return;
  }

  // Ensure note is under correct date note
  const parentDate = underWhichDateNote(note);
  if (parentDate === date) {
    return;
  }

  clearParentDateNotes(note);
  const dateNote = api.getDateNote(date);
  if (dateNote == null) {
    return;
  }

  api.ensureNoteIsPresentInParent(note.noteId, dateNote.noteId, "Meeting");
};
