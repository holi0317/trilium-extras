<!--
Trilium template for a new meeting notes

To install this note, copy the content of this note. Then do following configuration:

1. Add following labels
  - `#meetingNote`
  - `#label:Date=promoted,single,date`
  - `#label:Time=promoted,single,text`
  - `#label:Venue=promoted,single,text`
  - `#relation:ProjectEpic=promoted,single`
  - (optional) `#iconClass="bx bxs-chat"`
2. Add relation `~runOnAttributeChange` and point that to "Attribute changed" note
3. Delete this header

Note type: Text
-->

## Participants

-

## Notes

## Actionable items

- [ ]

## Next meeting
