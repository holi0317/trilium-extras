/**
 * Plugin for trilium notes.
 *
 * This will add/delete clones of meeting notes when some attributes has changed.
 *
 * Type: JS Backend
 * Required attributes: (None)
 *
 * Please add an relation on "Meeting notes template", `~runOnAttributeChange` to this script.
 *
 * It is expected that this note will have 2 child notes, `dateHandler` and `epicHandler`.
 */

if (api.originEntity == null) {
  return;
}

const note = api.originEntity.getNote();

if (note == null) {
  return;
}

// Ensure the note triggering this script is really a meeting note
if (!note.hasLabel("meetingNote")) {
  return;
}

// ==== Ensure note is under `#meetingNoteRoot` ====
const meetingRootNote = api.getNoteWithLabel("meetingNoteRoot");
if (meetingRootNote == null) {
  api.log("Cannot find #meetingNoteRoot");
  return;
}

api.ensureNoteIsPresentInParent(note.noteId, meetingRootNote.noteId);

// ======= Date handling =======
dateHandler(note);

// ======= Epic handling =======
epicHandler(note);
