/**
 * Plugin for trilium notes.
 *
 * This will add a "New meeting" button on the toolbar
 *
 * Type: JS Frontend
 * Required attributes:
 *   - `#run=frontendStartup`
 */

api.addButtonToToolbar({
  title: "New meeting",
  icon: "chat",
  async action() {
    const meetingNoteID = await api.runOnBackend(() => {
      const meetingRootNote = api.getNoteWithLabel("meetingNoteRoot");
      if (meetingRootNote == null) {
        api.log("Cannot find #meetingNoteRoot");
        return null;
      }

      const resp = api.createTextNote(
        meetingRootNote.noteId,
        "New meeting",
        ""
      );

      return resp.note.noteId;
    });

    if (meetingNoteID == null) {
      return;
    }

    // wait until the frontend is fully synced with the changes made on the backend above
    await api.waitUntilSynced();

    // we got an ID of newly created note and we want to immediately display it
    await api.activateNewNote(meetingNoteID);
  },
});
