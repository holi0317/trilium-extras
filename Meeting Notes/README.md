# Widget description

This widget provides meeting notes organization

# Behaviors

- Each meeting notes have a date, time, venue and a project epic
- All meeting notes will be stored under `#meetingNoteRoot`
- The meeting note will be cloned to the day note of the date specified
- The meeting note will be cloned to "Meeting notes" folder under the specified project epic
  - The project epic is a relationship
  - This "Meeting notes" folder will be created automatically if the epic does not have that folder

# Assumptions

- There will only be 0 or 1 epic per meeting note
- A meeting notes cannot have multiple parent notes that are date note.
  The parent date note must align with `#Date` label in the meeting notes
  - However, the meeting notes can have other parents that are not date note.

# Label used

| Label name         | Description                                            | Single or multiple |
| ------------------ | ------------------------------------------------------ | ------------------ |
| `#meetingNote`     | A meeting note                                         | Multiple           |
| `#meetingNoteRoot` | Folder containing all meeting notes                    | Single             |
| `#meetingEpicRoot` | Folder that should contain meeting notes under an epic | Multiple           |

## Label on `#meetingNote`

| Label name     | Description                      |
| -------------- | -------------------------------- |
| `#Date`        | Date of the meeting              |
| `#Time`        | Time of the meeting              |
| `#Venue`       | Venue of the meeting             |
| `~ProjectEpic` | Epic for this note to belongs to |

# Install instruction

![Example trilium notes structure](<./Example structure.png>)

All scripts/template have a header section containing how to install them. Please
refer to those instruction when installing.

This section only list out the install order.

1. (Recommended) Create a "Meeting notes" folder under "widget" folder
2. (Recommended) Create a "Implementation" folder under "Meeting notes"
3. Install "New meeting button" script
4. Install "Attribute changed" script and it's child scripts (dateHandler, epicHandler)
5. Install Meeting notes template script
6. Create a folder "Meeting notes store" under "widget/Meeting notes" folder

   1. In fact the name and location does not matter. But following steps are important
   2. Add a relation `~child:template` on the folder that points to "Meeting notes template"
   3. Add attribute `#meetingNoteRoot` to the folder

7. Reload trilium
